#!/usr/bin/env python

import asyncio, websockets, math, json

def input_int(msg: str, opt: int):
    try:
        value = input(msg)
        if value:
            return int(value) if value != "x" else value
        else:
            return opt
    except ValueError:
        return opt

async def setNewConfig(client):
    print("========================================")
    
    label = input("> Set label:\n")
    minutes = input_int("> Set minutes (30):\n", 30)
    seconds = input_int("> Set seconds (0):\n", 0)
    if seconds > 60:
        minutes += math.ceil(seconds / 60)
        seconds = seconds % 60

    audioFile = input("Set audio file:\n")
    
    await client.send(json.dumps({
            "label": label,
            "minutes": minutes,
            "seconds": seconds,
            "audioFile": audioFile
        }))

    print()

async def configPlugin(client, path):
    print()

    quit = False
    while not quit:
        try:
            await setNewConfig(client)
        except EOFError:
            quit = True
            asyncio.get_event_loop().stop()

ws_server = websockets.serve(configPlugin, "localhost", 8080)

print("Waiting for client...")
asyncio.get_event_loop().run_until_complete(ws_server)
asyncio.get_event_loop().run_forever()
